# About the documentation

Afivo is documented using Doxygen, with comments inside the code and separate
markdown files. A good way to read the documentation is
through [this link](http://cwimd.nl/other_files/afivo_doc/html/index.html).
Alternatively, if you have a recent version of Doxygen installed, you
can locally generate the documentation with:

    $ make doc
    $ firefox documentation/html/index.html # Or other browser
